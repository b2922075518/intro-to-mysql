CREATE TABLE students(

	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)

);

CREATE TABLE subjects(

	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR (100) NOT NULL,
	PRIMARY KEY(id)

);

CREATE TABLE enrollments(

	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	datetime_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id),
	CONSTRAINT fk_enrollments_student_id 
		FOREIGN KEY(student_id)
		REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id 
		FOREIGN KEY(subject_id)
		REFERENCES subjects(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	

);

INSERT INTO students(username, password, full_name, email)
	VALUES("adrian", "password", "Adrian Tamayo", "test@mail.com")
;

INSERT INTO subjects(course_name, schedule, instructor)
	VALUES("Java Short Course", "M-F 5:30PM-9:30PM", "Ms. Camille Doroteo")
;

INSERT INTO enrollments(student_id, subject_id)
	VALUES(1, 1)
;


