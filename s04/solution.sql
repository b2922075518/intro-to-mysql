-- Find all artists that has letter "d" in its name:
SELECT * FROM artists 
WHERE name LIKE "%d%"
; 


-- Find all songs that has a length of less than 330:
SELECT * FROM songs 
WHERE length < 330
;


-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.):
SELECT al.album_title, s.song_name, s.length 
FROM albums al
JOIN songs s
ON al.id = s.album_id
;


-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.):
SELECT * FROM artists ar
JOIN albums al
ON ar.id = al.artist_id
WHERE al.album_title LIKE "%a%"
;


-- Sort the albums in Z-A order. (Show only the first 4 records.):
SELECT * FROM albums
ORDER BY album_title DESC
LIMIT 4
;


-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums
JOIN songs
ON albums.id = songs.album_id
ORDER BY album_title DESC
;


-- Join the 'artists' and 'albums' tables. (Find all artists that has letter a in its name):
SELECT * FROM artists ar
JOIN albums al
ON ar.id = al.artist_id
WHERE ar.name LIKE "%a%"
;