-- Add New Records
-- Add 4 More Artists (artist 8, 9, 10, 11)
INSERT INTO artists(name) 
	VALUES("Lady Gaga"), ("Justin Bieber"), ("Ariana Grande"), ("Bruno Mars")
;

-- Taylor Swift (artist 5)
-- Fearless Album (album 5)
-- Add 2 More Songs (song 7, 8)

INSERT INTO songs(song_name, length, genre, album_id) 
	VALUES("Fearless", "00:04:06", "Pop Rock", 5),
			("Love Story", "00:03:33", "Country Pop", 5)
;

-- Create a new album for Taylor Swift (artist 5)
INSERT INTO albums(album_title, date_released, artist_id)
	VALUES("Red", "2012-10-22", 5)
;
-- Add a song to the album Red
INSERT INTO songs(song_name, length, genre, album_id) 
	VALUES("State of Grace", "00:04:33", "Rock, Alternative Rock, Arena Rock", 8)
;
INSERT INTO songs(song_name, length, genre, album_id) 
	VALUES("Red", "00:03:24", "Country", 8)
;

-- Lady Gaga (artist 8)
INSERT INTO albums(album_title, date_released, artist_id)
	VALUES("A Star Is Born", "2018-10-05", 8)
;
INSERT INTO albums(album_title, date_released, artist_id)
	VALUES("Born This Way", "2011-05-23", 8)
;

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES("Black Eyes", "00:03:01", "Rock and roll", 9),
			("Shallow", "00:03:21", "Country, Rock, Folk Rock", 9)
;

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES("Born This Way", "00:04:12", "Electropop", 10)
;

--Justin Bieber (artist 9)
